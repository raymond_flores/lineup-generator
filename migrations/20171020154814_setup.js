exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('players', function(table) {
      table.integer('id');
      table.string('team', 3);
      table.string('position', 3);
      table.decimal('projections');
      table.smallint('week');
      table.smallint('season');
      table.jsonb('data');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('players')
  ]);
};
