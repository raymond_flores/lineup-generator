exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('stats', function(table) {
      table.integer('player_id');
      table.integer('week');
      table.integer('season');
      table.json('data');
    }),
    knex.schema.createTable('dfs_points', function(table) {
      table.integer('player_id');
      table.integer('week');
      table.integer('season');
      table.json('data');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('stats'),
    knex.schema.dropTable('dfs_points')
  ]);
};
