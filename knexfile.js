// Update with your config settings.
var config = require('./db-config.json');
module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: config.database,
      user:     config.user,
      password: config.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  },
  staging: {

  },
  production: {
    client: 'pg',
    connection: {
      database: config.database,
      user:     config.user,
      password: config.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  }
};
