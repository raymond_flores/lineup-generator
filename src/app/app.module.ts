import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatGridListModule, MatSliderModule, MatProgressSpinnerModule, MatToolbarModule, MatIconModule, MatChipsModule, MatInputModule} from '@angular/material';

import {AppComponent} from './app.component';
import {PlayersComponent} from './players/players.component';
import { LineupComponent } from './lineup/lineup.component';
import { PlayerRowComponent } from './players/player-row/player-row.component';
import {Api} from './services/api/api.service';
import { PlayerPositionPipe } from './pipes/player-position.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PlayersComponent,
    LineupComponent,
    PlayerRowComponent,
    PlayerPositionPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSliderModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatChipsModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [Api],
  bootstrap: [AppComponent]
})
export class AppModule {
}
