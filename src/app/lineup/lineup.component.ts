import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lineup',
  templateUrl: './lineup.component.html',
  styleUrls: ['./lineup.component.css']
})
export class LineupComponent implements OnInit {

  @Input() lineup;

  constructor() { }

  ngOnInit() {
  }

}
