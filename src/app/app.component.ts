import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Api} from './services/api/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DFS Tool!';
  players = null;
  lineup = null;
  filters = { oprk: 1, ork: 1, proj: 0, removedPlayers: []};
  positionFilter = 'All';
  fruits = [
    { name: 'Lemon' },
    { name: 'Lime' },
    { name: 'Apple' },
  ];

  constructor(private http: Http, public api: Api) {}

  ngOnInit(): void {
    // Make the HTTP request:
    this.api.get('/api/players')
      .map((res:Response) => res.json())
      .subscribe((data) => {
        console.log(data);
        this.players = data.players;
        this.filters = data.filters;
      });
  }

  updateFilter(event, filter) {
    console.log(event, filter);
    this.filters[filter] = event.value;
  }

  getPlayers(event) {
    console.log(this.filters)
    this.api.get('/api/players', this.filters)
      .map((res:Response) => res.json())
      .subscribe(data => this.players = data.players);
  }

  generateLineup() {
    this.api.get('/api/players/create-lineup', this.filters)
      .map((res:Response) => res.json())
      .subscribe(data => this.lineup = data);
  }

  changePositionFilter(pos) {
    this.positionFilter = pos;
  }
}
