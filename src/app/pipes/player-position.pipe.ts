import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'PlayerPosition'
})
export class PlayerPositionPipe implements PipeTransform {

  transform(items: any[], filter: Object): any {
    if (!items || !filter || (filter && filter === 'All')) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter(item => item.position === filter);
  }

}
