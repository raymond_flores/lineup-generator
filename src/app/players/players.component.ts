import { Component, OnInit, Input } from '@angular/core';
import { PlayerRowComponent } from './player-row/player-row.component';

@Component({
  selector: 'players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  @Input() players;
  @Input() removedPlayers;


  constructor() {
  }

  ngOnInit() {
    console.log(this.removedPlayers);

  }
}
