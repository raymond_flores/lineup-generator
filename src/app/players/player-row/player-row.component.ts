import { Component, Input } from '@angular/core';

@Component({
  selector: '[player-row]',
  templateUrl: './player-row.component.html',
  styleUrls: ['./player-row.component.scss']
})
export class PlayerRowComponent {

  @Input() player;
  @Input() removedPlayers;

  constructor() { }

  gameDetails(player) {
    if (player.HomeOrAway === 'HOME')
      return player.Opponent + '@<b>' + player.Team + '</b>';
    else
      return '<b>' + player.Team + '</b>@' + player.Opponent;
  }

  removePlayer(playerId) {
    if (this.removedPlayers.indexOf(playerId) === -1) this.removedPlayers.push(playerId);
    else this.removedPlayers.splice(this.removedPlayers.indexOf(playerId), 1);
  }
}
