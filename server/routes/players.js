const express = require('express');
const router = express.Router();
const pg = require('../services/pg');

const defaultFilters = {
  ork: 16, // Opponent Rank
  oprk: 22, // Opponent Position Rank
  proj: 0, // Projections
  removedPlayers: []
};

/* GET api listing. */
router.get('/', (req, res) => {
  getPlayers(req.query, (err, players) => {
    if (!err) res.json({ players: players, filters: defaultFilters });
    else res.status(500).send();
  });
});

router.get('/create-lineup', (req, res) => {
  getPlayers(req.query, (err, players) => {
    if (!err) res.json(knapsack(players, 'ceil'));
    else res.status(500).send();
  });
});

function getPlayers(params, callback) {
  var filters = Object.assign({}, defaultFilters, params);
  console.log(filters);
  pg.query("select id, projections::float, " +
    "(select round(avg((data->>'FantasyPointsDraftKings')::float)::decimal,2)::float from dfs_points where player_id = players.id) as points, " +
    "(select min((data->>'FantasyPointsDraftKings')::float)from dfs_points where player_id = players.id) as floor, " +
    "(select max((data->>'FantasyPointsDraftKings')::float) from dfs_points where player_id = players.id) as ceil, " +
    "data->>'Name' as name, (data->>'DraftKingsSalary')::int as salary, position, " +
    "(data->>'OpponentPositionRank')::int as oprk, " +
    "(data->>'OpponentRank')::int as ork, " +
    "(projections / ((data->>'DraftKingsSalary')::float / 1000)) as cpp, " +
    "data " +
    "from players where week = $5 and data->>'DraftKingsSalary' is not null and (data->>'OpponentPositionRank')::int >= $1 " +
    "and data->>'Team' != ALL($3) and data->>'Status\' != 'Out' " +
    "and id != all($6) " +
    "and (data->>'OpponentRank')::int >= $2 and projections >= $4 order by salary desc", [filters.oprk, filters.ork, ['OAK', 'MIA', 'DET', 'GB'], filters.proj, new Date().getNFLWeek(), "{" + filters.removedPlayers + "}"], function (err, result) {
    if (!err) callback(null, result.rows);
    else callback('Error');
  });
}

function totalPoints(team) {
  return team.reduce(function(a, b) {
    return a + b.points;
  }, 0);
}

function createLineup(players) {
  var lineup = {
    QB: null,
    RB1: null,
    RB2: null,
    WR1: null,
    WR2: null,
    WR3: null,
    TE: null,
    FLEX: null,
    DEF: null
  };

  for (var i in players) {
    if (['QB', 'DEF'].indexOf(players[i].position) !== -1) lineup[players[i].position] = players[i];
    else if (players[i].position === 'RB') {
      if (!lineup.RB1) lineup.RB1 = players[i];
      else if (!lineup.RB2) lineup.RB2 = players[i];
      else lineup.FLEX = players[i];
    }
    else if (players[i].position === 'WR') {
      if (!lineup.WR1) lineup.WR1 = players[i];
      else if (!lineup.WR2) lineup.WR2 = players[i];
      else if (!lineup.WR3) lineup.WR3 = players[i];
      else lineup.FLEX = players[i];
    }
    else if (players[i].position === 'TE') {
      if (!lineup.TE) lineup.TE = players[i];
      else lineup.FLEX = players[i];
    }
  }
  return lineup;
}

function knapsack(players, valueCategory) {
  var budget = 50000;
  var current_salary = 0;
  var team = [];
  var constraints = {
    'QB': 1,
    'RB': 2,
    'WR': 3,
    'TE': 1,
    'DEF': 1,
    'FLEX': 1
  };
  var counts = {
    'QB': 0,
    'RB': 0,
    'WR': 0,
    'TE': 0,
    'DEF': 0,
    'FLEX': 0
  };

  players.sort(function(a, b) {
    return (b[valueCategory] / b.salary) - (a[valueCategory] / a.salary);
  });

  for (var i in players) {
    var pos = players[i].position;
    var sal = players[i].salary;

    if (counts[pos] < constraints[pos] && current_salary + sal <= budget) {
      team.push(players[i]);
      counts[pos]++;
      current_salary += sal;
    } else if (counts['FLEX'] < constraints['FLEX'] && current_salary + sal <= budget && ['RB', 'WR', 'TE'].indexOf(pos) !== -1) {
      team.push(players[i]);
      counts['FLEX']++;
      current_salary += sal;
    }
  }

  players.sort(function(a, b) {
    return b[valueCategory] - a[valueCategory];
  });
  team.sort(function(a, b) {
    return (a[valueCategory] / a.salary) - (b[valueCategory] / b.salary);
  });

  for (var j in team) {
    var pos = team[j].position;
    var sal = team[j].salary;
    var pts = parseFloat(team[j][valueCategory]);

    var pos_players = players.filter(function(player) {
      return player.position === pos && player[valueCategory] > pts && team.indexOf(player) === -1;
    });
    pos_players.sort(function(a, b) {
      return b[valueCategory] - a[valueCategory];
    });

    for (var k in pos_players) {
      if (((current_salary + pos_players[k].salary - sal) <= budget) && (parseFloat(pos_players[k][valueCategory])) > pts) {
        team[j] = pos_players[k];
        current_salary = current_salary + pos_players[k].salary - sal;
        break;
      }
    }
  }
  return { team: createLineup(team), salary: current_salary, points: totalPoints(team) };
}

module.exports = router;
