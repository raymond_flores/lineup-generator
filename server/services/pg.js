const pg = require('pg');
var config = require('../../db-config.json');
var conString = "postgres://" + config.user + ":" + config.password + "@localhost/" + config.database;

function query(queryString, args, callback) {
  pg.connect(conString, function (err, client, done) {
    if (err) {
      console.error(err);
      if (callback) callback('error fetching client from pool' + err, null);
    }

    client.query(queryString, args, function (err, result) {
      //call `done()` to release the client back to the pool
      done();

      if (err) {
        console.error(err);
        console.log(queryString);
        if (callback) callback('error running query' + err, null);
      }
      else
      if (callback) callback(null, result);
    });

  });
}

function commitTransaction (client, done) {
  console.log('Committing Transaction...');
  client.query('COMMIT', done);
}

var rollback = function(client, done) {
  console.log('Rollback Transaction...');
  client.query('ROLLBACK', function(err) {
    //if there was a problem rolling back the query
    //something is seriously messed up.  Return the error
    //to the done function to close & remove this client from
    //the pool.  If you leave a client in the pool with an unaborted
    //transaction weird, hard to diagnose problems might happen.
    return done(err);
  });
};

function startTransaction (callback) {
  pg.connect(conString ,function(err, client, done) {
    if(err) throw err;
    console.log('Begin Transaction...');
    client.query('BEGIN', function(err) {
      if(err) return rollback(client, done);
      //as long as we do not call the `done` callback we can do
      //whatever we want...the client is ours until we call `done`
      //on the flip side, if you do call `done` before either COMMIT or ROLLBACK
      //what you are doing is returning a client back to the pool while it
      //is in the middle of a transaction.
      //Returning a client while its in the middle of a transaction
      //will lead to weird & hard to diagnose errors.
      process.nextTick(function() {
        callback(client, done);
      });
    });
  });
}

function queryTransaction(client, done, queryString, args, callback) {
  client.query(queryString, args, function (err, result) {
    if (err) {
      console.error(err);
      if (callback) callback('error running query' + err, null);
      return rollback(client, done);
    }
    else {
      if (callback) callback(null, result);
    }
  });
}

module.exports = {
  query: query,
  CONNECTION_STRING: conString,
  startTransaction: startTransaction,
  queryTransaction: queryTransaction,
  commitTransaction: commitTransaction,
  rollback: rollback
};
