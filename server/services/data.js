const request = require('request');
const pg = require('./pg');
const API_KEY = 'f6e71921b21c4919bb579a4eff04c320';

function updatePlayers() {
  var options = {
    url: 'https://api.fantasydata.net/v3/nfl/stats/JSON/DailyFantasyPlayers/' + getNFLWeekDate(),
    headers: {
      'Ocp-Apim-Subscription-Key': API_KEY
    }
  };
  request(options, function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var players = JSON.parse(body);
      players.map(function (player, index) {
        updatePlayer(player);
      });
    }
    else
      console.log(error)
  });
}

function updatePlayer(player, cb) {
  pg.query('SELECT EXISTS(SELECT 1 FROM players WHERE id = $1 AND week = $2 AND season = $3)', [player.PlayerID, new Date().getNFLWeek(), 2017], function (err, result) {
    if (!err) {
      if (!result.rows[0].exists) {
        pg.query('INSERT INTO players (id, position, projections, data, week, season, team) VALUES ($1, $2, $3, $4, $5, $6, $7)', [player.PlayerID, player.Position, player.ProjectedFantasyPoints, player, new Date().getNFLWeek(), 2017, player.Team], function (insertErr) {
          if (insertErr) {
            console.log('Insert:', insertErr);
            if (cb) cb();
          }
          else if (cb) cb();
        });
      }
      else {
        pg.query('UPDATE players SET position = $2, projections = $3, data = $4, team = $7 WHERE id = $1 and week = $5 and season = $6', [player.PlayerID, player.Position, player.ProjectedFantasyPoints, player, new Date().getNFLWeek(), 2017, player.Team], function (updateErr) {
          if (updateErr) {
            console.log('Update:', updateErr);
            if (cb) cb();
          }
          else if (cb) cb();
        });
      }
    }
    else console.log(err);
  });
}

function getAllPlayerStats() {
  var options = {
    url: 'https://api.fantasydata.net/v3/nfl/stats/JSON/PlayerGameStatsByWeek/2017/' + new Date().getNFLWeek(),
    headers: {
      'Ocp-Apim-Subscription-Key': API_KEY
    }
  };
  console.log('Update All Players Week:', new Date().getNFLWeek());
  request(options, function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var players = JSON.parse(body);
      console.log('Players:', players.length);
      players.map(function (player) {
        savePlayerStats(player);
      });
    }
    else console.log(error)
  });
}

function savePlayerStats(player) {
  pg.query('SELECT EXISTS(SELECT 1 FROM stats WHERE player_id = $1 AND week = $2 AND season = $3)', [player.PlayerID, player.Week, player.Season], function (err, result) {
    if (!err) {
      if (!result.rows[0].exists) {
        pg.query('INSERT INTO stats (player_id, week, season, data) VALUES ($1, $2, $3, $4)', [player.PlayerID, player.Week, player.Season, player], function (insertErr) {
          if (insertErr)
            console.log('Insert:', insertErr);
          // else
          //     console.log('Player Inserted', player.PlayerID);
        });
      }
      else {
        pg.query('UPDATE stats SET data = $2 WHERE player_id = $1 and week = $3 and season = $4', [player.PlayerID, player, player.Week, player.Season], function (updateErr) {
          if (updateErr)
            console.log('Update:', updateErr);
          // else
          //     console.log('Player Updated', player.PlayerID);
        });
      }
    }
    else
      console.log(err);
  });
}

function getDFSPoints() {
  var options = {
    url: 'https://api.fantasydata.net/v3/nfl/stats/JSON/DailyFantasyPoints/' + getNFLWeekDate(),
    headers: {
      'Ocp-Apim-Subscription-Key': API_KEY
    }
  };
  request(options, function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      var players = JSON.parse(body);
      console.log(players.length);
      players.map(function (player, index) {
        saveDFSPoints(player, new Date().getNFLWeek(), 2017);
      });
    }
    else
      console.log(error)
  });
}

function saveDFSPoints(player, week, season) {
  pg.query('SELECT EXISTS(SELECT 1 FROM dfs_points WHERE player_id = $1 AND week = $2 AND season = $3)', [player.PlayerID, week, season], function (err, result) {
    if (!err) {
      if (!result.rows[0].exists) {
        pg.query('INSERT INTO dfs_points (player_id, week, season, data) VALUES ($1, $2, $3, $4)', [player.PlayerID, week, season, player], function (insertErr) {
          if (insertErr)
            console.log('Insert:', insertErr);
          // else
          //     console.log('Player Inserted', player.PlayerID);
        });
      }
      else {
        pg.query('UPDATE dfs_points SET data = $2 WHERE player_id = $1 and week = $3 and season = $4', [player.PlayerID, player, week, season], function (updateErr) {
          if (updateErr)
            console.log('Update:', updateErr);
          // else
          //     console.log('Player Updated', player.PlayerID);
        });
      }
    }
    else
      console.log(err);
  });
}

function getNFLWeekDate() {
  // returns tuesday of current NFL week
  return new Date(new Date('09-04-2017').getTime() + (86400000 * 2 + (604800000 * (new Date().getNFLWeek() - 1)))).toISOString().slice(0, 10);
}

Date.prototype.getNFLWeek = function () {
  // The number of milliseconds in one week
  var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
  // Convert both dates to milliseconds
  var date1_ms = this.getTime() - 18000000; // 5 hours in milliseconds;
  var date2_ms = new Date('08-29-17'); // 1 week before first tuesday of NFL season
  // Calculate the difference in milliseconds
  var difference_ms = date1_ms - date2_ms;
  // Convert back to weeks and return hole weeks
  var week = Math.floor(difference_ms / ONE_WEEK);
  return week > 0 ? week : 1;
};

updatePlayers();
